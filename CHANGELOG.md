# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v2.12.1-SNAPSHOT] [r4.24.0] - 2020-06-18

**Bug fixes**

[#19479] Add Resource facility does not work


## [v2.12.0] - 2019-11-13

[Feature #18039] Ckan-Util-Library: read the vre-public-url into GR "CkanPortlet"


## [v2.10.0] - 2019-08-01

[Task #16895] Try to patch some methods of the jackan library


## [v2.9.0] - 2019-04-11

[Task #16471] Migrate to new Catalogue Resolver


## [v2.8.1] - ${buildDate}

Added title on updated of records refs #16395


## [v2.8.1] - 2019-02-26

Fixed Bug on social post publishing


## [v2.8.0] - 2018-11-20

Patched method DataCatalogueImpl#getPortletURL


## [v2.7.0] - 2018-10-10

Added the possibility to deny social post on catalogue-ws #12514


## [v2.6.0] - 2018-07-18

Enhance timeout in requests to CKAN in ckan-util-library #12050


## [v2.5.0] - 2018-02-28

Code cleanup


## [v2.4.2] - 2018-02-28

Portlet url is built on the fly (#11294)


## [v2.4.1] - 2018-01-10

Minor fix for delegates

Added support for statistics (requires solrj)

Added method for retrieving main landing pages


## [v2.4.0] - 2017-11-01

Added delegate roles method

Added search methods for datasets


## [v2.3.2] - 2017-08-01

Minor fixes


## [v2.3.1] - 2017-07-01

Mime type fix for resources


## [v2.3.0] - 2017-05-01

Pom fixes

Moved to java 8

Added method to add a dataset to a group and its parents (if any)


## [v2.2.1] - 2017-04-01

Some minor changes


## [v2.2.0] - 2017-02-01

Added method to delete/purge group

Project structure and so packages are changed (gwt-widget like structure)


## [v2.1.0] - 2016-12-01

Product creation methods improved

Added method to get the group list

Added methods to manage groups

Added method to patch product/resource


## [v2.0.0] - 2016-11-01

Added support for datasets's relationships (create, delete, retrieve methods)

Added Factory in order to reduce the number of created instances of the utils class(cache)

Added code to discover a new Application Profile (see ticket #4925)

Added methods to manage group creation, assing a user/product to a group


## [v1.0.0] - 2016-06-01

First Release
